export interface UserObj{
    avatar: string,
    email: string,
    first_name: string,
    id: string,
    last_name: string
  }