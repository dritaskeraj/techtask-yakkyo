import Vue from 'vue'
import VueRouter, { NavigationGuardNext, Route, RouteConfig } from 'vue-router'
import SignIn from '../views/SignIn.vue'
import Users from '../views/Users.vue'
import Signup from '../views/Signup.vue'
import User from '../views/User.vue'

Vue.use(VueRouter)

async function beforeEnter(to: Route, from: Route, next: NavigationGuardNext<Vue>) {
  if (localStorage.getItem('access-token')) next()
  else next({ name: 'SignIn' })
}

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'SignIn',
    component: SignIn
  },
  {
    path: '/users',
    name: 'Users',
    beforeEnter,
    component: Users
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/user/:id',
    name: 'User',
    component: User,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
