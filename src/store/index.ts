import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentUser: {},
    allUsers: []
  },
  mutations: {
    SET_CURRENT_USER(state, user) {
      state.currentUser = user;
    },
    SET_USERS(state, users){
      state.allUsers = users;
    }
  },
  actions: {
    setCurrentUser(context, user) {
      context.commit('SET_CURRENT_USER', user);
    },
    setUsers(context, users){
      context.commit('SET_USERS', users);
    }
  }
})
